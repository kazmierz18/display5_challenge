/**
 * kazmierz18
 */
struct C {
  static const constexpr int value = __LINE__;
}; // this code must be in line 5 of this file

#include <cassert>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

#define assertIsFive(x)                                                        \
  do {                                                                         \
    assert(5 == (x));                                                          \
    std::cout << (x) << "\n";                                                  \
  } while (0)

int main(int argc, char const *argv[]) {
  {
    // get five using sizeof struct
    struct FiveFromStruct {
      int32_t a;
      char b;
    } __attribute__((packed)); // pack struct to prevent from adding padding

    assertIsFive(sizeof(FiveFromStruct));
  }
  {
    // get five using bitwise or
    assertIsFive((sizeof(int32_t) | sizeof(char)));
  }
  {
    // get five using table of chars
    const char fiveFromTable[] = "abcd";

    assertIsFive(sizeof(fiveFromTable));
  }
  {
    // get five using std::string
    const std::string fiveFromString = "abcdf";

    assertIsFive(fiveFromString.length());
    assertIsFive(std::strlen(fiveFromString.c_str()));
  }
  {
    // get five using vector
    std::vector<char> vec{'a', 'a', 'a', 'a', 'a'};

    assertIsFive(vec.size());
  }
  {
    // get five using __LINE__ (look up 5th line of this file)
    assertIsFive(C::value);
  }
  {
    // get line using constant values from std includes
    assertIsFive((int)(std::numeric_limits<double>::round_error() * 10));
  }
  {
    // get  five using bitfield
    union FiveFromBitField {
      struct BitField {
        bool b1 : 1, : 1, b2 : 1;
      } __attribute__((packed));
      BitField bitField;
      int value;
    };

    FiveFromBitField b;
    b.value = 0;
    b.bitField.b1 = true;
    b.bitField.b2 = true;

    assertIsFive(b.value);
  }
  {
    // get five using enum
    enum class FiveFromEnum { a, d, f, g, h, u, j, k };

    assertIsFive((int)FiveFromEnum::u);
  }
  return 0;
}
